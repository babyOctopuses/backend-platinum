'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('UserGameBiodata', 'username', {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    });

    await queryInterface.addColumn('UserGameBiodata', 'password', {
      type: Sequelize.STRING,
      allowNull: false
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('UserGameBiodata', 'username', {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    });

    await queryInterface.removeColumn('UserGameBiodata', 'password', {
      type: Sequelize.STRING,
      allowNull: false
    });
  }
};
