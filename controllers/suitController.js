module.exports = {
  home: (req, res) => {
    res.render("suit-game", {
      title: "game",
    });
  },
  getPoint: (req, res) => {
    UserGameHistory.create({
      win: req.body.win,
      score: req.body.score,
    })
      .then((user) => {
        res.status(201).json(user);
      })
      .catch((err) => {
        res.status(404).json(`Failed to update`);
      });
  },
};
