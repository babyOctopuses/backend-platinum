const { UserGameBiodata } = require("../models");

function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  // AUTH USER

  register: (req, res) => {
    UserGameBiodata.register(req.body)
      .then((usergamebiodata) => {
        res.status(201).json(usergamebiodata);
      })
      .catch((err) => {
        res.status(422).json("Can't create register data");
      });
  },

  login: (req, res) => {
    UserGameBiodata.authenticate(req.body).then((user) => {
      res.json(format(user));
    });
  },

  // PROFILE USER

  middlewareUpdateProfile: (req, res) => {
    const currentUser = req.user;
    res.json(currentUser);
  },

  //Update Profile
  updateProfile: (req, res) => {
    UserGameBiodata.update(
      {
        fullname: req.body.fullname,
        gender: req.body.gender,
      },
      {
        where: { userID: req.params.userID },
      }
    )
      .then((user) => {
        res.status(201).json(user);
      })
      .catch((err) => {
        res.status(404).json("error", err);
      });
  },
};
