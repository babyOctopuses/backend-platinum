const express = require("express");
const router = express.Router();
const user = require("./user");
const suit = require("./suit");

router.use('/api/user', user);

module.exports = router;
