const express = require("express");
const router = express.Router();
const suitController = require("../controllers/suitController");

router.get("/gamesuit", suitController.home);
router.post("/getPoint", suitController.getPoint);

module.exports = router;
